# 本地测试
Pod::Spec.new do |s|
  # 项目名
  s.name             = 'ZMVenders'
  # 版本号
  s.version          = '0.0.2'
  # 简述
  s.summary          = '第三方组件库'
  # 详述
  s.description      = <<-DESC
    TODO: 在这里添加pod的详细描述。
  DESC

  # 项目主页
  s.homepage         = 'https://blog.csdn.net/weixin_38633659'
  # 项目需要遵守的协议
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  # 作者邮箱
  s.author           = { 'chenzimin' => '1005332621@qq.com' }
  # 项目最低支持版本
  s.ios.deployment_target = '9.0'

  # ===================  所有子项目  =======================
  s.subspec 'AFNetworking' do |ss|
    # 3.1.0
    ss.source_files = 'Products/AFNetworking/*.{h,hpp}'
    ss.public_header_files = 'Products/AFNetworking/*.{h,hpp}'
    ss.vendored_library = 'Products/AFNetworking/*.a'
  end

  s.subspec 'IQKeyboardManager' do |ss|
    # 3.3.7
    ss.source_files = 'Products/IQKeyboardManager/*.{h,hpp}'
    ss.public_header_files = 'Products/IQKeyboardManager/*.{h,hpp}'
    ss.vendored_library = 'Products/IQKeyboardManager/*.a'
  end

  s.subspec 'libwebp' do |ss|
    # 0.6.1
    ss.source_files = 'Products/libwebp/*.{h,hpp}'
    ss.public_header_files = 'Products/libwebp/*.{h,hpp}'
    ss.vendored_library = 'Products/libwebp/*.a'
  end

  s.subspec 'MJRefresh' do |ss|
    # 3.1.15.7
    ss.source_files = 'Products/MJRefresh/*.{h,hpp}'
    ss.public_header_files = 'Products/MJRefresh/*.{h,hpp}'
    ss.vendored_library = 'Products/MJRefresh/*.a'
    ss.resources = 'Products/MJRefresh/MJRefresh.bundle'
  end
  # ===================  所有子项目  =======================

  
  # git仓库地址
  s.source           = { :git => 'https://gitee.com/chenzm_186/ZMVenders.git', :tag => s.version.to_s }
  # 设置默认下载实例,没有设置则下载所有
  # All
  s.default_subspec='All'
  s.subspec 'All' do |ss|
    ss.dependency 'ZMVenders/AFNetworking'
    ss.dependency 'ZMVenders/IQKeyboardManager'
    ss.dependency 'ZMVenders/libwebp'
    ss.dependency 'ZMVenders/MJRefresh'
  end

end
# ************************************************************
