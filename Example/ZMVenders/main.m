//
//  main.m
//  ZMVenders
//
//  Created by rattanchen on 12/08/2020.
//  Copyright (c) 2020 rattanchen. All rights reserved.
//

@import UIKit;
#import "ZMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ZMAppDelegate class]));
    }
}
