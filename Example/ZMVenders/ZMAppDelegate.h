//
//  ZMAppDelegate.h
//  ZMVenders
//
//  Created by rattanchen on 12/08/2020.
//  Copyright (c) 2020 rattanchen. All rights reserved.
//

@import UIKit;

@interface ZMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
