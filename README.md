# ZMVenders

[![CI Status](https://img.shields.io/travis/rattanchen/ZMVenders.svg?style=flat)](https://travis-ci.org/rattanchen/ZMVenders)
[![Version](https://img.shields.io/cocoapods/v/ZMVenders.svg?style=flat)](https://cocoapods.org/pods/ZMVenders)
[![License](https://img.shields.io/cocoapods/l/ZMVenders.svg?style=flat)](https://cocoapods.org/pods/ZMVenders)
[![Platform](https://img.shields.io/cocoapods/p/ZMVenders.svg?style=flat)](https://cocoapods.org/pods/ZMVenders)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ZMVenders is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ZMVenders'
```

## Author

rattanchen, 1005332621@qq.com

## License

ZMVenders is available under the MIT license. See the LICENSE file for more info.
